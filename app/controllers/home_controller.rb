class HomeController < ApplicationController
  def index
    @tweets = Tweet.distinct(:user_id).includes(user: :company).order("tweets.created_at DESC").group('tweets.user_id').first(20)
  end
end
